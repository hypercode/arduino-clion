
#include "Arduino.h"

void setup() {
    pinMode(13, OUTPUT);
    Serial.begin(9600);
    Serial.println("Hello world!");
}

bool a = 1;

void loop() {
    a = !a;
    digitalWrite(13, a);
    delay(1000);
}
